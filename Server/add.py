'''
This script adds entry to the database Series.
This script is used by Admin to reset the database to default value.
'''
from google.appengine.ext import db
import webapp2
import json
import logging

class Series(db.Model):
    tvid = db.StringProperty(required=True)
    title = db.StringProperty(required=True)
    status = db.IntegerProperty()
    epname = db.StringProperty()
    epinfo = db.StringProperty()
    epdate = db.StringProperty()
    rely = db.IntegerProperty()
    up_cycle = db.IntegerProperty()

class Add2Series(webapp2.RequestHandler):
    def get(self):
        logging.info("GET request received")
        query = Series.all()
        query.order("tvid") 

        for q in query.run():
            self.response.write(q.tvid)
            self.response.write("\t")
            self.response.write(q.title)
            self.response.write("\t")
            self.response.write(q.status)
            self.response.write("\n")
            self.response.write(q.epname)
            self.response.write("\n")
            self.response.write(q.epinfo)
            self.response.write("\n")
            self.response.write(q.epdate)
            self.response.write("\n")
            self.response.write(q.rely)
            self.response.write("\n")

        logging.info("Request Successfully Executed")

    def post(self):
        logging.info("POST Request received")
        jsbody = self.request.body
        data = json.loads(jsbody)

        rtvid = data['tvid']
        rname = data['name']
        rstat = data['status']
        repname = data['epname']
        repinfo = data['epinfo']
        repdate = data['epdate']
        rrely = data['rely']
        rup_cycle = data['up_cycle']
        
        logging.debug("name = " + rname)
        logging.debug("tvid = " + str(rtvid))
        logging.debug("status = " + str(rstat))
        logging.debug("epname = " + repname)
        logging.debug("epinfo = " + str(repinfo))
        logging.debug("epdate = " + str(repdate))
        logging.debug("rely = " + str(rrely))
        logging.debug("up_cycle = " + str(rup_cycle))

        '''              
        series = Series.all()
        series.filter('tvid = ', str(rtvid))
        entry = series.get()
        '''

        entry = Series(tvid=rtvid, title=rname)
        
        entry.epname = repname
        entry.epinfo = repinfo
        entry.epdate = repdate
        entry.rely = rrely
        entry.status = rstat
        entry.up_cycle = rup_cycle
        
        entry.put()

        logging.debug("Entry added to Database")
        logging.info("Request successfully executed")

        self.response.write("Added " + rtvid)


application = webapp2.WSGIApplication([("/add", Add2Series),],debug=True)
