#Module to get running status of a series given its imdb id
#It takes IMDb id of any series as input and returns the latest episode object.

def getNextEp(tvid):
    import imdb
    import re
    from datetime import *
    from IMDbDate import formatDate

    print "initialized"
    i = imdb.IMDb()

    series = i.get_movie(tvid)
    print series['title']
    if(series['kind'] == 'tv series'):
        i.update(series, "episodes")
        print "updated"

        series = series["episodes"]

        # taking up first episode of the last season and last episode of the second last
        # season to determine the present season

        # for some series, -1 key appears in the end of the list
        if(series.has_key(-1)):
          series.pop(-1);

        season_seclast = series[len(series) -1]
        season_last = series[len(series)]

        #in case of naruto the first episode doesn't have the key 1. Rather it is 25.
        # so the keys are sorted and then the smallest key is chosen. 
        if(season_last.has_key(1)):
            fdate = season_last[1]["original air date"]
        else:
            fdate = season_last.get(sorted(season_last.keys())[0])["original air date"]

        pdate = season_seclast[len(season_seclast)]["original air date"]

        bitch_present = re.split("-", str(date.today()))

        bitch_past = formatDate(pdate)
        bitch_future = formatDate(fdate)

        ypa = bitch_past[0][0]
        ypr = bitch_present[0]
        yfu = bitch_future[0][0]

        mpa = bitch_past[0][1]
        mpr = bitch_present[1]
        mfu = bitch_future[0][1]

        dpa = bitch_past[0][2]
        dpr = bitch_present[2]
        dfu = bitch_future[0][2]

        flag = 0
        if(ypa < ypr):
            flag = 1
        elif(ypa == ypr):
            if(mpa == 0):
                pass
            else:
                if(mpa < mpr):
                    flag = 1
                elif(mpa == mpr):
                    if(dpa == 0):
                        pass
                    else:
                        if(dpa < dpr):
                            flag = 1

        if(flag == 1):
            if(ypr < yfu):
                flag = 2
            elif(ypr == yfu):
                if(mfu == 0):
                    flag = 2
                else:
                    if(mpr < mfu):
                        flag = 2
                    elif(mpr == mfu):
                        if(dfu == 0):
                            pass
                        else:
                            if(dpr < dfu):
                                flag = 2
        if(flag == 0):
            status = "Sec Last"
            current_season = season_seclast
        elif(flag == 1):
            status = "Last"
            current_season = season_last
        elif(flag == 2):
            status = "on Hold"
            

        print status

        if(flag == 2):
            nextEp = season_last[sorted(season_last.keys())[0]]
            nextEp["status"] = 0
            nextEp["datediff"] = 2
            
        else: 
            for k in current_season.keys():
                forDate = formatDate(current_season[k]["original air date"])
                current_season[k]["date"] = forDate[0]
                dt = current_season[k]["date"]
                if(forDate[1] == 1 or forDate[1] == 2):
                    current_season[k]["datediff"] = 0
                else:
                    num = 365*(int(dt[0]) - int(ypr)) + 31*(int(dt[1]) - int(mpr)) + (int(dt[2]) - int(dpr))
                    current_season[k]["datediff"] = num/abs(num)

            keySet = sorted(current_season.keys())

            start = keySet[0]
            n = keySet[len(keySet) - 1]

            while(n >= start):
                if(current_season[n]["datediff"] == -1):
                  lastKnown = n
                  break
                n -= 1

            if(n != (start + len(keySet) -1)):
                nextEp = current_season[n+1]
                nextEp["status"] = 1
            else:
                nextEp = "No info about Season"
        return nextEp
    else:
        print "given imdb id is not a tv series"
