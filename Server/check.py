'''
Script to check whether the IMDb engine deployed on Appengine is working or not. 
'''
import webapp2
import imdb
import logging
from NextEp import getNextEp

class MainPage(webapp2.RequestHandler):

    def get(self):
    	logging.info("GET request received")
        id = "0903747"
        self.response.write(id)
        episode = getNextEp(id)
        self.response.write(episode['series title'])
        self.response.write(episode['title'])
        print episode['title']

application = webapp2.WSGIApplication([('/check', MainPage),],debug=True)
