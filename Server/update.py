# This script updates the database on getting a GET request by CRON jobs.

import webapp2
from google.appengine.ext import db
import logging
import imdb
from NextEp import getNextEp
'''
    Iter class contains only a single entry containing a value between 1 - 15 corresponding
    to different series. On every update event, this entry gets updated by 1.
'''

class Iter(db.Model):
    iterator = db.IntegerProperty(required=True)

'''
    This class contains the main database of the application. In this database, there are following entries

    tvid    : IMDb id of the the Series
    title   : Title of the Series
    epname  : Name of the next Episode
    epinfo  : Season And Episode Number in format S.E
    epdate  : Date of the Next episode as available on IMDb servers
    status  : Running Status of the series. Is the series running or on hold.
    0 implies series is on hold
    1 implies series is running
    rely    : This value tells us if the date present on IMDb is complete
    0 implies incomplete information is present. Either Month and Year or just Year
    1 implies complete information is present Date, Month and Year
    2 implies that series is on hold
'''
class Series(db.Model):
    tvid = db.StringProperty(required=True)
    title = db.StringProperty(required=True)
    status = db.IntegerProperty()
    epname = db.StringProperty()
    epinfo = db.StringProperty()
    epdate = db.StringProperty()
    rely = db.IntegerProperty()
    up_cycle = db.IntegerProperty()

    '''
    This class contains the Series whose next episodes are to be found out.
    '''
class ListSeries(db.Model):
    tvid = db.StringProperty(required=True)
    slno = db.IntegerProperty()
    name = db.StringProperty(required=True)


class UpdateSeries(webapp2.RequestHandler):
    '''
    On receiving a GET request (From a cron job mainly) these steps are undertaken

    1. Extractor iterator value for Iter database
    2. Corresponding to the iterator value, extract the movie ID from ListSeries class
    3. Extract all the information of the series and update the database
    '''
    def get(self):
        logging.info("Received a GET Request")

        # Get iterator value
        it = Iter.all().get()
        num = it.iterator

        if(num == 15):
            num = 0

        num = num+1
        it.iterator = num
        it.put()

        # Now get the corresponding id from ListSeries

        list_k = db.Key.from_path('ListSeries', str(num - 1))
        serial = ListSeries.get(list_k)
        stvid = serial.tvid
        logging.info("IMDb ID " + stvid)
        self.response.write(stvid)   

        episode = getNextEp(stvid)

        epNum = episode["episode"]
        seaNum = episode["season"]
        epName = episode["episode title"]
        epStat = episode["status"]
        epRely = episode["datediff"]
        epDate = episode["original air date"]   

        logging.info("Episode Number " + str(epNum))
        logging.info("Season Number " + str(seaNum))
        logging.info("Episode Title " + epName)
        logging.info("Status " + str(epStat))
        logging.info("Reliable " + str(epRely))
        logging.info("Next Episode date " + epDate)

        Ser = Series.all()
        Ser.filter('tvid = ', str(stvid))
        ser1 = Ser.get()

        logging.info("entry extracted from datastore")

        ser1.status = epStat
        ser1.rely = epRely
        ser1.epname = epName
        ser1.epinfo = str(seaNum) + "." + str(epNum)
        ser1.epdate = epDate
        ser1.up_cycle = ser1.up_cycle + 1

        ser1.put()

        logging.info(" Updated data written to Datastore")

        logging.info("New iterator value : " + str(num + 1))
        '''
        On receiving a POST request, the Iteratoe is reset to value 1
        '''
    def post(self):
        logging.info("POST Request received for Update")
        i = Iter(iterator=1).put()
        logging.info("iterator reset to 1")
        self.response.write("Reset to 1")


application= webapp2.WSGIApplication([("/update",UpdateSeries),],debug=True)
