'''
This script updates the user preferences based on the information sent via POST request
The POST request should contain the following keys
1. regid
2. preflist

Steps undertaken on receiving a POST request are
1. Extract Data of post request.
2. Extract the entry corresponding to the regid from UserPrefs DataBase.
   And overwrite present preferences and put back in database.

'''
import webapp2
import logging
import json
from google.appengine.ext import db

class UserPrefs(db.Model):
        regid = db.StringProperty(required=True)
        grey = db.IntegerProperty() #Grey's Anatomy
        bones = db.IntegerProperty() #Bones
        himym = db.IntegerProperty() #How I Met Your Mother
        dexter = db.IntegerProperty() #Dexter
        burn = db.IntegerProperty() #Burn Notice
        bbt = db.IntegerProperty() # The Big Band Theory
        bbad = db.IntegerProperty() # The Breaking Bad
        naruto = db.IntegerProperty() # Naruto Shippuden
        mentl = db.IntegerProperty() # The Mentalist
        castle = db.IntegerProperty() # Castle
        glee = db.IntegerProperty() # Glee
        vamp = db.IntegerProperty() # The Vampire's Diary
        walk = db.IntegerProperty() # The Walking Dead
        suits = db.IntegerProperty() # Suits
        home = db.IntegerProperty() # Homeland

class SetPref(webapp2.RequestHandler):
    def post(self):
        logging.info('POST request received')
        one = False
        # Step 1. Extract data from POST request
        
        reqBody = self.request.body
        body = json.loads(reqBody)

        if(not (body.has_key('regid')) and not (body.has_key('preflist'))):
            logging.error('Improper POST request: Key "regid" and "preflist" absent')
            self.response.write("Improper Request")
        elif(not body.has_key('regid')):
            logging.error('Improper POST request: Key "regid" absent')
            self.response.write("Improper Request")
        elif(not body.has_key('preflist')):
            logging.error('Improper POST request: Key "preflist" absent')
            self.response.write("Improper Request")
        else:
            r_regid = body['regid']
            r_preflist = body['preflist']

            logging.info('regid : ' + r_regid)
            logging.info('preflist : ' + r_preflist)

            list_series = ['home', 'suits', 'walk', 'vamp', 'glee', 'castle',
                           'mentl', 'naruto', 'bbad', 'bbt', 'burn', 'dexter',
                           'himym', 'bones', 'grey']
            if(len(r_preflist) != 15):
                logging.error("Improper preference length (15) : " + str(len(r_preflist)))
                self.response.write("Improper preference")

            else:
                preflist = int(r_preflist)
                pref = {}

                for l in list_series :
                        bit = preflist%10
                        if(bit == 0 or bit == 1):
                                pref[l] = bit
                                preflist/=10
                        else:
                                logging.error("Improper preference digit (0,1) : " + str(bit))
                                self.response.write("Improper Preference")
                                break
                one = True
                
        # Step 1 completed
        
        # Step 2. Extract entry from db UserPrefs
        # corresponding to the regid and overwrite earlier preferences
        # Step 2 is undertaken only when step 1 succeeded

        if(one == True):
            q = UserPrefs.all()
            q.filter('regid =', r_regid)
            entry = q.get()

            if(entry == None):
                logging.error("No entry matched with given regid. User not registered.")
                self.response.write('Not Registered')
            else:
                entry.home = pref['home']
                entry.suits = pref['suits']
                entry.walk = pref['walk']
                entry.vamp = pref['vamp']
                entry.glee = pref['glee']

                entry.castle = pref['castle']
                entry.mentl = pref['mentl']
                entry.naruto = pref['naruto']
                entry.bbad = pref['bbad']
                entry.bbt = pref['bbt']

                entry.burn = pref['burn']
                entry.dexter = pref['dexter']
                entry.himym = pref['himym']
                entry.bones = pref['bones']
                entry. grey= pref['grey']
                
                entry.put()
                logging.debug("Entry written to DataBase")
                self.response.write('Preferences Set')
        # Step 2 Completed
        
    def get(self):
        logging.info('GET request received')
        self.response.write('Link to Set Preferences')
        
application = webapp2.WSGIApplication([('/setpref', SetPref),],debug=True)
