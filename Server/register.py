'''
This script registers users whose regid is sent via a post request
Only regid key is to be sent via post request
They are added to UserPrefs Database with default prefences none

'''
import webapp2
import json
from google.appengine.ext import db
import logging

class UserPrefs(db.Model):
        regid = db.StringProperty(required=True)
        grey = db.IntegerProperty() #Grey's Anatomy
        bones = db.IntegerProperty() #Bones
        himym = db.IntegerProperty() #How I Met Your Mother
        dexter = db.IntegerProperty() #Dexter
        burn = db.IntegerProperty() #Burn Notice
        bbt = db.IntegerProperty() # The Big Band Theory
        bbad = db.IntegerProperty() # The Breaking Bad
        naruto = db.IntegerProperty() # Naruto Shippuden
        mentl = db.IntegerProperty() # The Mentalist
        castle = db.IntegerProperty() # Castle
        glee = db.IntegerProperty() # Glee
        vamp = db.IntegerProperty() # The Vampire's Diary
        walk = db.IntegerProperty() # The Walking Dead
        suits = db.IntegerProperty() # Suits
        home = db.IntegerProperty() # Homeland

class Register(webapp2.RequestHandler):
        def post(self):
                logging.info("POST request received")
                reqBody = self.request.body
                body = json.loads(reqBody)
                # Check if the POST request is proper
                if(body.has_key("regid")):
                        rregid = body['regid'] # Received Registration ID from User

                        logging.info("Received Registration ID " + rregid)

                        q = UserPrefs.all()
                        q.filter("regid =", rregid)
                        entry = q.get()

                        # Check whether the User is already registered or not

                        if(entry == None):

                                i = UserPrefs(regid=rregid)
                                
                                i.grey = 0
                                i.bones = 0
                                i.himym = 0
                                i.dexter = 0
                                i.burn = 0
                                i.bbt = 0
                                i.bbad = 0
                                i.naruto = 0
                                i.mentl = 0
                                i.castle = 0
                                i.glee = 0
                                i.vamp = 0
                                i.walk = 0
                                i.suits = 0
                                i.home = 0

                                i.put()

                                logging.info("New User Registered")

                                self.response.write("registered")
                        else:
                                self.response.write("already registered")
                                logging.error("Already Registered")
                else:
                        logging.error("Improper POST request. No 'regid' key")

        def get(self):
                logging.info("GET request received")
                self.response.write("Registration Link")

application = webapp2.WSGIApplication([('/register', Register),],debug=True)
